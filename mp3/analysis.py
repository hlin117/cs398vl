"""
This is where all of the other fun stuff begins.

We now take the dialogue list, and hash each person's name's
to a dictionary ranking their "score" of their interactions.

The score is determined by sentiment analysis.
"""
from colorama import *
from sentiment import Model
import json

class Relation():

	def __init__(self, alphaString):
		split = alphaString.split(" ")
		self.person1 = split[0]
		self.person2 = split[1]
		
		self.score = 0
		self.scoreSum = 0
		self.numQuotes = 0
		self.numDialogues = 0
	
	def __repr__(self):
		return self.toDict() 
	
	# Returns a string representation of oneself
	def stringify(self):
		return json.dumps(self.toDict())

	def toDict(self):
		output = dict()
		output["person1"] = self.person1
		output["person2"] = self.person2
		output["score"] = self.score
		output["numDialogues"] = self.numDialogues
		output["numQuotes"] = self.numQuotes
		return output

	# Adds a dialogue to this object
	def addScore(self, score, talkLength):
		self.scoreSum += float(score)
		self.numDialogues += 1
		self.numQuotes += talkLength
		self.updateScore()

	def updateScore(self):
		self.score = float(self.scoreSum) / self.numDialogues

def createOutputDict(dialogueList):
	names = set()
	outputDict = dict()

	print Fore.GREEN + "Training classifier..." + Fore.RESET
	model = Model()
	print Fore.GREEN + "Done." + Fore.RESET

	# Returns an output dictionary 
	for dialogue in dialogueList:
		speaker1 = dialogue["speaker1"]
		speaker2 = dialogue["speaker2"]
		talklist = dialogue["talklist"]
		names.add(speaker1)
		names.add(speaker2)
		alphaString = getAlpha(speaker1, speaker2)
		
		# Updates the output dictionary
		relation = outputDict.get(alphaString, None)
		if relation is None:
			relation = Relation(alphaString)	
		score = model.analyze(talklist)
		relation.addScore(score, len(talklist))
		
		outputDict[alphaString] = relation	

	
	for key in outputDict.keys():
		outputDict[key] = outputDict[key].toDict()

	return (list(names), outputDict)

# Alphabetically sorts speaker1 and speaker2 and puts them in a string
def getAlpha(speaker1, speaker2):
	if speaker1 == speaker2:
		print Fore.YELLOW + "WARNING: Hashed names are the same for " + speaker1 + Fore.RESET
	speaker1 = speaker1.replace(" ", "-")
	speaker2 = speaker2.replace(" ", "-")
	if speaker1 < speaker2:
		return speaker1 + " " + speaker2
	else:
		return speaker2 + " " + speaker1
