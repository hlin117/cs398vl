General algorithm:

-- Training the model for sentiment analysis --
1. Need to get a dataset which has "good", "bad" and "neutral" sentences
2. Input this into the nltk sentiment analysis API

-- Obtain the actual data --
1. Tokenize the sentences
2. Find out the named entities. Map "similar" named entities to the same word.
3. Remove irrelavent characters, classify people who have only talked once to not have talked at all
4. Run each sentence of a conversation through the sentiment analysis model. Assign person u and v a score from that analysis. 
5. After we parse the whole entire book, we output json per chapter. Each json object will contain the following:

	1. Person A
	2. Person B
	3. How many times they've spoke (number of sentences that include both of them)
	4. Their "overall happiness with each other."

	Between person A and person B, there is only one of these objects (don't double count)

-- The visualization --
Each character A and B will have a line between them (provided they met the "threshold").
The color of their line depends on whether they are happy with each other.
	If the color is red, then they have common negative interactions.
	If the color is green, then they have (overall) a positive interaction.
Intensities of the red/green-ness depends how "polar" the opinion is.

For each one of the json files (that represents a "snapshot" of the interactions between the characters),
we could have a slider on the bottom that allows you to change the graph data to that snapshot.
