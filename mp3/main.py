#!/usr/bin/python
import types
import json, re, nltk
import os
from colorama import *
from nltk import Tree
from analysis import createOutputDict
import unicodedata

quote = Fore.GREEN + "QUOTE" + Fore.RESET
verbNoun = Fore.GREEN + "VERBNOUN" + Fore.RESET
nphrase = Fore.GREEN + "NPHRASE" + Fore.RESET
verbPronoun = Fore.GREEN + "VERBPRONOUN" + Fore.RESET

g_names = dict()     # Global set of names
speakerErrs = 0
dialogueErrs = 0
detectSpeakErrs = 0
ambiSpeakers = 0
numJunkDialogue = 0
origNumDialogue = 0
SHOWAMBISPEAKERS = False
SHOWDETECTSPEAKERRS = False
SHOWDIALOGUE = False
SHOWSPEAKERS = False

quoteNum = 0
mruDictionary = dict()



def main():
	book = "ofk-ch4"


	initializeDictionary()
	paragraphs = getCleanedParagraphs(book)
	paraTrees = getParagraphTrees(paragraphs)
	dialogueList = getDialogueList(paraTrees)
	dialogueList = removeNoise(dialogueList)

	# Now have to analyze the current data, and ship it off as a json object
	(names, relations) = createOutputDict(dialogueList)
#	table = createOutputTable(relations)
	output = createOutput(relations, names)

	# Outputting things
	with open(book + ".table", "w") as outfile:
		json.dump(relations, outfile, indent = 4, ensure_ascii = False)
	with open(book + ".json", "w") as outfile:
		json.dump(output, outfile, indent = 4, ensure_ascii = False)

	print "-------------------- RESULTS -----------------------"
	print "Had to automatically choose a speaker " + str(speakerErrs) + " times." 
	print "Had " + str(dialogueErrs) + " number of dialogue errors." 
	print "Couldn't detect speakers " + str(detectSpeakErrs) + " times."
	print "Had " + str(ambiSpeakers) + " occurrances of ambiguous speakers."
	print "Threw out " + str(numJunkDialogue) + " dialogue instances from an original of " + str(origNumDialogue)
	print Fore.GREEN + "In total, parsed " + str(quoteNum) + " quotes."

# Initializes g_names so we have a set of pre-defined people!
def initializeDictionary():
	namesList = ["Wart", "Pellinore", "Ector", "Kay", "Lancelot", \
				"Grummore", "Merlyn", "Arthur"]

	# Chapter 2
	namesList.extend(["Toirdealbhach", "Morlan", "Gael", "Palomides"])

	# Chapter 3
	namesList.extend(["Dop", "Pelles", "Brisen", "Pelles", "Bliant", "Gawaine", "Mador", \
	                  "Meliagrance"])

	# Chapter 4
	namesList.extend(["Agravaine"])

	for name in namesList:
		g_names[name] = 0
"""
# Creates an output table consisting of the sentiments of each character
def createOutputTable(relations):
	table = [dict(item[0], item[1].toDict()) for item in relations.items()]
	return table
"""
"""
Creates the output that should go into the cluster

relations is a dictionary of relations. You can key into
each relation using the "name1 name2" scheme
"""
def createOutput(relations, names):
	output = []
	keys = relations.keys()

	for name in names:
		node = dict()
		node["name"] = name
		pairs = set()
		for key in keys:
			if name in key:
				pairs.add(key)
	
		node["imports"] = [getOtherName(name, pair) for pair in pairs]	
		output.append(node)

	return output 

# Splits a pair string, and gets the other
# half of the string that doesn't include the name
def getOtherName(currname, pair):
	arr = pair.split(" ")
	if currname in arr[0]:
		return arr[1]
	return arr[0]

# Converts each one of the parsed, tagged paragraphs into a
# grammar tree. The grammar tree will help us figure out what is a conversation
# later on
def getParagraphTrees(paragraphs):
	grammar = quote + ": {<``><.*>+?<''>}"
	grammar += "\n" + verbNoun + ": {<V.*><DT>*<NNP>+|<DT>*<NNP>+<V.*>}"
	grammar += "\n" + nphrase + ": {<NNP>+}"
	grammar += "\n" + verbPronoun + ": {<PRP><V.*>}"
	parser = nltk.RegexpParser(grammar)

	paraTrees = []
	for para in paragraphs:
		try:
			tree = parser.parse(para)
			paraTrees.append(tree)
		except:
			print "Failed sentence on: " + para
	return paraTrees


"""
If a tree doesn't have any quote subtree, then that means that it does
not add to the dialogue. However, any proper noun mentioned inside
the paragraph may be the speaker for the next paragraph

We need to keep states of who were are speakers and stuff 

Case 1: There is no quote to be read. Then maybe we have a set-up
paragraph that introduces the speakers of the next paragraph.

Case 2: There is a quote, and a speaker. Associate this quote with
this speaker.

Case 2.5: There is a personal pronoun

Case 3: There is a quote, but no speaker. We have to backtrack
to other paraTrees to find out who the speaker is.

If there is no speaker for two paragraphs, assume that the conversation
has ceased.
"""
def getDialogueList(paraTrees):
	global g_names
	global detectSpeakErrs
	global ambiSpeakers
	dialogueList = []

	talkList = []
	speakerSet = set()
	noQuotes = 0

	for i in range(0, len(paraTrees)):
		currTree = paraTrees[i]
	
		# If the tree has no quote, then it might be setting up for another paragraph.
		if not hasQuote(currTree):
			phrase = getPhrase(currTree) # for debugging
			if len(talkList) > 0:
				noQuotes += 1

			# Case when we add to the talklist
			if noQuotes == 2:
				try:
					dialogue = dict()
					dialogue["speaker1"] = talkList[0][0]
					dialogue["speaker2"] = talkList[1][0]
					i = 1

					# Keep on iterating until speaker2 changes so we have two different speakers
					while dialogue["speaker2"] == dialogue["speaker1"]:
						i += 1 # TODO: THIS IS ACTUALLY A MISTAKE. You can't increment an iterator
						dialogue["speaker2"] = talkList[i][0]

					dialogue["talklist"] = talkList
					if type(dialogue) is nltk.tree.Tree: 
						print "Putting a dialogue that is not a dict"
					dialogueList.append(dialogue)
				except:
					global dialogueErrs
					dialogueErrs += 1
					if SHOWDIALOGUE:
						print Fore.YELLOW + "Failed on a dialogue. Talklist is as such: "
						print json.dumps(talkList, indent = 4) + Fore.RESET

				# Resetting the state
				talkList = []
				speakerSet = set()
				noQuotes = 0
			continue
	
		noQuotes = 0

		if hasSpeaker(currTree):
			speaker = getSpeaker(currTree)		# abstracts cleaning away
			quote = getQuote(currTree)
			talkList.append((speaker, quote))
			addToSet(speakerSet, speaker)

		# There is a quote, but no speaker
		# OR there is a pronoun
		else:
			try:
				if len(speakerSet) > 2:
					ambiSpeakers += 1
					if SHOWAMBISPEAKERS:
						print Fore.RED + "Warning: Speakerset is more than 3. " + Fore.RESET + str(speakerSet)

				prevPrevSpeaker = talkList[len(talkList) - 2][0]
				quote = getQuote(currTree)
				talkList.append((prevPrevSpeaker, quote))

			except:
				detectSpeakErrs += 1	
				if SHOWDETECTSPEAKERRS:
					print "Failed on " + str(getPhrase(currTree))
	return dialogueList	

# Because NLTK has bugs, and we want to map as any names to a given speaker,
# we need to do a cleaning procedure on this
def addToSet(speakerSet, speaker):

	# Checks for "extra appending speaker" bugs
	for xSpeaker in speakerSet:

		# Case 1: xSpeaker is an "incorrect" object that we have to change
		if speaker in xSpeaker:
			speakerSet.discard(xSpeaker)
			speakerSet.add(speaker)
			return speaker

		# Case 2: speaker is an "incorrect" object we have to change... Just don't add it.
		elif xSpeaker in speaker:
			speaker = xSpeaker
			break

	speakerSet.add(speaker)

# Cleans the speaker out of tailing words and stuff	
def cleanSpeaker(speaker):
	speaker = speaker.replace("Sir ", "")
	speaker = speaker.replace("King ", "")
	speaker = speaker.replace("Uncle ", "")
	speaker = speaker.replace("St. ", "")
	speaker = cleanPhrase(speaker, removePeriod = True, removeQuotes = True)	
	return speaker


def addCount(speaker):
	global g_names
	global mruDictionary
	global quoteNum
	quoteNum += 1
	mruDictionary[speaker] = quoteNum 
	g_names[speaker] = g_names.get(speaker, 0) + 1


# Discards any "non-name" words in the speaker, which could
# or could not be dirty.
# TODO NOT FINISHED
def stemSpeaker(speaker):

	if speaker == "Sir Palomides":
		i = 2

	# Optimization: If the speaker is more than one word,
	# check if any of the words exist in the dictionary already
	speakArr = [cleanSpeaker(part) for part in speaker.split(' ')]
	for part in speakArr:
		if part in g_names:
			return part

	speaker = " ".join(speakArr).strip()

	# If it is a name we haven't discovered yet, or something
	# we can't splice, then we have to check substrings...
	for name in g_names.keys():

		# If the name is in speaker
		if name in speaker:
			return name

		# If we need to do a replacement
		if speaker in name:
			print Fore.YELLOW + "Replacing " + name + " with " + speaker + Fore.RESET
			del g_names[name]
			return speaker
	
	# Otherwise, this is a new speaker we haven't discovered yet
	return speaker

def hasQuote(tree):
	for subtree in tree.subtrees(filter = lambda tree : tree.node == quote):
		return True
	return False

def hasSpeaker(tree):
	subtrees = []
	for subtree in tree.subtrees(filter = lambda tree : tree.node == verbNoun):
		subtrees.append(subtree)

	names = [getName(tree) for tree in subtrees]
	names = [name for name in names if name != ""]
	return len(names) != 0

# Obtains the speaker in this tree. This function will also
# find the most "probable" speaker in this tree by looking
# at g_names. Hence, it does all of the filtering work no one
# wants to do
# TODO NOT FINISHED
def getSpeaker(tree):
	global speakerErrs
	speakList = []
	for subtree in tree.subtrees(filter = lambda tree : tree.node == verbNoun):
		name = getName(subtree)
		if name == "":
			continue
		speakList.append(name)

	if len(speakList) == 0:
		return None

# TODO: THIS MIGHT BE BAD. If we have trashy names, this could be a reason
#	speakList = [cleanSpeaker(speaker) for speaker in speakList]
	speakList = [stemSpeaker(speaker) for speaker in speakList]	
	speakList = [remapSpeaker(speaker) for speaker in speakList] 

	# Obtain a speaker only if it is in the g_names dictionary.
	# Does not consider the possibility if we throw out too many candidates	
	speakList = [speaker for speaker in speakList if speaker in g_names]
	if len(speakList) > 1:
		speakerErrs += 1
		if SHOWSPEAKERS:
			print Fore.RED + "Found more than one speaker: " + str(speakList)
			print "Occured on tree: " + str(tree) + Fore.RESET
#	speaker = cleanPhrase(speakList[0], removePeriod = True)
#	speaker = cleanSpeaker(speaker)
	addCount(speaker)
	return speaker

# There could be some common "name ambiguities" that the author uses.
# For example, "King" might refer to "king pellinore".  
# For this reason, we'll do a "most recently used" scheme
def remapSpeaker(speaker):
	if speaker == "King":
		candidate1 = mruDictionary.get("Pellinore", 0)
		candidate2 = mruDictionary.get("Pelles", 0)
		candidate3 = mruDictionary.get("Arthur", 0)
		maximum = max(candidate1, candidate2, candidate3)
		if maximum == candidate1:
			return "Pellinore"
		if maximum == candidate2:
			return "Pelles"
		if maximum == candidate3:
			return "Arthur"
		print "Couldn't map " + speaker + " to anything"
	return speaker

# TODO: Clean the quotes
def getQuote(tree):
	quoteList = []
	for subtree in tree.subtrees(filter = lambda tree : tree.node == quote):
		quoteList.append(getPhrase(subtree))
	_quote = " ".join(quoteList)
	return cleanPhrase(_quote, removeQuotes = True)


def cleanPhrase(phrase, removePeriod = False, removeQuotes = False):
	phrase = re.sub(r" ,", ",", phrase)
	phrase = re.sub(r"`` ", "\"", phrase)
	phrase = re.sub(r" \?", r"?", phrase)
	phrase = re.sub(r" n't", "n't", phrase)
	phrase = re.sub(r" \'s", "\'s", phrase)
	phrase = re.sub(r"- ", "-", phrase)
	phrase = re.sub(r" \.", ".", phrase)
	phrase = re.sub(r" ''", "\"", phrase)

	if removePeriod:
		phrase = phrase.replace(".", "")
	if removeQuotes:
		phrase = phrase.replace("\"", "")
	return phrase.strip()

def getPhrase(tree):
	flat = tree.flatten() # Returns a tree with leaves of (word, pos)
	posList = flat.leaves()	
	phrase = ""
	for pair in posList:
		phrase += pair[0] + " "
	return cleanPhrase(phrase)

# Obtains a name from a tree
# NOTE: MAY RETURN EMPTY STRING
def getName(tree):
	flat = tree.flatten()
	posList = flat.leaves()
	name = ""
	for pair in posList:
		if re.match('N.*', pair[1]):
			name += pair[0] + " "
	
	nameArr = name.split(" ")
	nameArr = [part for part in nameArr if re.match(r'[A-Z][a-z]+', part)] 
	name = " ".join(nameArr).strip()
	return name


# Gets rid of negligible people in the dialogue list
# If one of the speakers in a dialogue has a "number of times spoken"
# less than some threshold number, then we throw out that dialogue
def removeNoise(dialogueList, threshold = 5):
	global origNumDialogue
	global numJunkDialogue
	origNumDialogue = len(dialogueList)
	newList = [dialogue for dialogue in dialogueList if makesThreshold(dialogue, threshold) and noEmptySpeaker(dialogue) and isNotPronoun(dialogue)]
	numJunkDialogue = origNumDialogue - len(newList)
	return newList 

def isNotPronoun(dialogue):
	pattern = re.compile(r"He|She|The|It")
	return not pattern.match(dialogue["speaker1"]) \
		and not pattern.match(dialogue["speaker2"])

# TODO
def noEmptySpeaker(dialogue):
	return dialogue["speaker1"] != "" and dialogue["speaker2"] != ""

def makesThreshold(dialogue, threshold):
	return not (g_names.get(dialogue["speaker1"], 0) < threshold or g_names.get(dialogue["speaker2"], 0) < threshold)

def getCleanedParagraphs(book):

	# If a cached file exists on disk, just use it
	cleaned = os.path.join("ofk", book + ".cleaned")
	if os.path.exists(cleaned):
		print Fore.GREEN + "Reading cached file... " + Fore.RESET
		paragraphs = json.load(open(cleaned))
		outputlist = []
		for para in paragraphs:
			sublist = []
			for pair in para:
				word = unicodedata.normalize("NFKD", pair[0]).encode('ascii', 'ignore')
				tag = unicodedata.normalize("NFKD", pair[1]).encode('ascii', 'ignore')
				sublist.append((word, tag))
			outputlist.append(sublist)


		return outputlist#[[tuple(pair) for pair in para] for para in paragraphs]

	# Otherwise, we have to make one
	else:
		textpath = os.path.join("ofk", book + ".txt")
		text = str(open(textpath).read())
		paragraphs = text.split("\n\n")
		output = []
		for para in paragraphs:
			para = para.replace("\n", " ")			# Removing annoying endlines
			para = re.sub(r"n'[^a-z]", "ng ", para) 	# Removing differences in English
			para = nltk.tokenize.word_tokenize(para)
			para = nltk.pos_tag(para)
			output.append(para)
			
		with open(cleaned, "w") as outfile:
			print Fore.RED + "Writing to file " + cleaned + Fore.RESET
			json.dump(output, outfile, indent = 4, ensure_ascii = False)
		return output

if __name__ == "__main__":
	main()

