import nltk.classify.util
import nltk
from nltk.classify import NaiveBayesClassifier
from nltk.corpus import movie_reviews


class Model():

	# Source: http://streamhacker.com/2010/05/10/text-classification-sentiment-analysis-naive-bayes-classifier/
	#
	# For more information about the classifier:
	# http://www.nltk.org/_modules/nltk/classify/naivebayes.html 
	def __init__(self, cutoff = 1, data = None):
	
		if data is not None:
			pass
			# TODO Allow user to create more data

		negids = movie_reviews.fileids('neg')
		posids = movie_reviews.fileids('pos')
		 
		negfeats = [(word_feats(movie_reviews.words(fileids=[f])), 'neg') for f in negids]
		posfeats = [(word_feats(movie_reviews.words(fileids=[f])), 'pos') for f in posids]
		
		negcutoff = len(negfeats) * cutoff 
		poscutoff = len(posfeats) * cutoff
		 
		trainfeats = negfeats[:negcutoff] + posfeats[:poscutoff]
		testfeats = negfeats[negcutoff:] + posfeats[poscutoff:]
		 
		self.classifier = NaiveBayesClassifier.train(trainfeats)			# Trains the classifier!


	"""
	Performs sentiment analysis on a talklist, a list of
	tuples containing (speaker, quotes).

	Independent of the audience, analyze will weight a talklist's
	positive or negative score based upon the longer quotes in the 
	talklist. The intuition is that longer quotes are more "profound" and
	they give more meaning towards the text.
	"""
	def analyze(self, talklist):
		totalScore = 0
		totalNumWords = 0
		for pair in talklist:
			numWords = len(nltk.word_tokenize(pair[1]))
			totalNumWords += numWords
			output = self.classifier.classify(word_feats(pair[1]))	
			totalScore += numWords if output == "pos" else -1*numWords	
	
		try:
			return float(totalScore) / totalNumWords
		except:
			print "Divide by zero warning"
			return 0
	


def word_feats(words):
    return dict([(word, True) for word in words])



 
