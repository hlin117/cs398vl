"""
A python module to do python tree manipulations on sentences.
These are simple parse trees.
"""
import nltk, re
from nltk import Tree
from colorama import *
from nltk.tokenize import sent_tokenize, word_tokenize


"""
TODO: This module would be better suited as a class in the future.
Then we could define what kind of grammar we want based upon the
input parameters to the constructor
"""

prefixes = set(["sir", "queen", "king", "mr", "mrs", "ms", "dr"])
pronouns = set(["he", "she", "it", "we", "they"])
possessPronouns = set(["his", "her", "hers"])

# Some default colors
quote = Fore.CYAN + "QUOTE" + Fore.RESET
verbNoun = Fore.GREEN + "VERBNOUN" + Fore.RESET
nounPhrase = Fore.GREEN + "NPHRASE" + Fore.RESET
verbPronoun = Fore.BLUE + "VERBPRONOUN" + Fore.RESET
prepPhrase = Fore.YELLOW + "PREPPHRASE" + Fore.RESET
pronoun = Fore.BLUE + "PRONOUN" + Fore.RESET
conjunction = Fore.YELLOW + "CONJUNCTION" + Fore.RESET

grammar = quote + ": {<``><.*>+?<''>}"
grammar += "\n" + conjunction + ": {<CC><.*>*?(<,>|<.>)}"
grammar += "\n" + prepPhrase + ": {<IN><DT>*<NN.*>+}"
grammar += "\n" + verbNoun + ": {<V.*><DT>*<JJ>*<NN.*>+|<DT>*<JJ>*<NN.*>+<V.*>}"
grammar += "\n" + nounPhrase + ": {<DT>*<JJ>*<NN.*>+}" # Does not include verbs
grammar += "\n" + verbPronoun + ": {<PRP><V.*>}"
grammar += "\n" + pronoun + ": {<PRP>}"
parser = nltk.RegexpParser(grammar)

# Converts each one of the parsed, tagged paragraphs into a
# grammar tree. The grammar tree will help us figure out what is a conversation
# later on
def getParagraphTrees(paragraphs):

	paraTrees = [getParaTree(para) for para in paragraphs]
	return paraTrees

# Input is a tagged list of words. Returns a parse tree defined 
# by the grammar above
def getParaTree(para, grammar = None):
	global parser

	# TODO: Should have put this into a class, not a static module...
	if grammar is not None:
		nounPhrase = Fore.GREEN + "NPHRASE" + Fore.RESET
		smallConjunct = Fore.YELLOW + "CONJUNCT" + Fore.RESET
		possessNoun = Fore.RED + "POSSESSIVE" + Fore.RESET
		longVerb = Fore.MAGENTA + "LONGVERB" + Fore.RESET
		verbPhrase = Fore.RED + "VERBPHRASE" + Fore.RESET
		description = Fore.BLUE + "DESCRIPT" + Fore.RESET
		pastPart = Fore.BLUE + "PARTICIPLE" + Fore.RESET

		grammar = possessNoun + ": {<PRP\$><JJ>*<NN.*>}"
		grammar += "\n" + description + ": {(<PRP>|<NNP>)<VBD>(<RB>*<JJ>|<TO><VB.*><NN.*>*)}"
		grammar += "\n" + nounPhrase + ": {<DT>*<RB.*>*<JJ>*<NN.*>+(<WP><V.*>(<IN><NN.*>)*)*}"
		grammar += "\n" + smallConjunct + ": {<CC><.*>*?(<IN>|<.>|<,>)}"
		grammar += "\n" + pastPart + ": {<RB>*<VB.*><RB>*<V.*>+}"
		parser = nltk.RegexpParser(grammar)
			
	return parser.parse(para)
#print Fore.RED + "Failed sentence on: " + para + Fore.RESET

"""
Below are operations to use on quoted passages.
"""
def hasQuote(tree):
	for subtree in tree.subtrees(filter = lambda tree : tree.node == quote):
		return True
	return False


# TODO: I really need to refactor this class =(
def getPhrases(tree, * pos):
	nounPhrase = Fore.GREEN + "NPHRASE" + Fore.RESET
	smallConjunct = Fore.YELLOW + "CONJUNCT" + Fore.RESET
	possessNoun = Fore.RED + "POSSESSIVE" + Fore.RESET
	longVerb = Fore.MAGENTA + "LONGVERB" + Fore.RESET
	verbPhrase = Fore.RED + "VERBPHRASE" + Fore.RESET
	description = Fore.BLUE + "DESCRIPT" + Fore.RESET
	pastPart = Fore.BLUE + "PARTICIPLE" + Fore.RESET

#	types = set()
#	if "adj" in pos:
#		types.union([possessNoun, description, nounPhrase])

	# TODO: More things with this	
	
	# Again, I could compact this into one line, but it'd be horrendously long...
	subtrees = []
	for subtree in tree.subtrees(filter = lambda subtree : subtree.node == description):
		subtrees.append(subtree)

	for subtree in tree.subtrees(filter = lambda subtree : subtree.node == possessNoun):
		subtrees.append(subtree)

	for subtree in tree.subtrees(filter = lambda subtree : subtree.node == nounPhrase):
		subtrees.append(subtree)
	
	return subtrees

# Identifies whether there is a speaker in the tree.
# Does this by checking the existence of "verbNouns"
def hasSpeaker(tree):
	names = getNameArray(tree, _nounPhrase = False)
	return len(names) != 0

# Obtains the speaker in this tree. 
def getSpeaker(tree):
	names = getNameArray(tree, _nounPhrase = False)
	return names[0] if len(names) != 0 else None

"""
Generic library functions
"""
# Returns an array of names in the following tree.
# By default, will try to get as many names as it can
# WILL NOT get pronouns.
def getNameArray(tree, _verbNoun = True, _nounPhrase = True):

	subtrees = []
	for subtree in tree.subtrees(filter = lambda tree : (tree.node == verbNoun and _verbNoun) 
			or (tree.node == nounPhrase and _nounPhrase)):
		subtrees.append(subtree)

	names = [getName(subtree) for subtree in subtrees]
	names = [name for name in names if name != ""]
	return names

# Checks whether a passage starts with a pronoun
def startsWithPronoun(tree):
	for subtree in tree.subtrees(filter = lambda tree : 
			tree.node == pronoun or tree.node == verbNoun or tree.node == nounPhrase):
		if getName(subtree) is not None:
			return False
		return True # If it's not a real name, then it's a pronoun
"""
		flat = subtree.flatten()
		posList = flat.leaves()
		for pair in posList:
			if re.match('N.*', pair[1]):
				return False
			if re.match('PRP', pair[1]):
				return True
		return False
"""
# NOTE: Must use a sentence tokenizer here!
def startsWithName(tree):
	flat = tree.flatten()
	posList = flat.leaves()
	index = 0 
	for i in range(0, len(posList)):
		if re.match('N.*', posList[i][1]):
			return True
		if posList[i][1] == ".":
			index = i
			break
	return False

def getFirstName(tree):
	flat = tree.flatten()
	posList = flat.leaves()
	index = 0 
	for pair in posList:
		if re.match('N.*', pair[1]):
			return pair[0]
		if pair[1] == ".":
			index = i
			break
	return None

"""
Operations below this line pertain to translating the tree into
human readable text.
"""

# Meshes all the quotes together of a paragraph. Disregards any other parts of speech.
def getQuote(tree):
	quoteList = []
	for subtree in tree.subtrees(filter = lambda tree : tree.node == quote):
		quoteList.append(getParagraph(subtree))
	_quote = " ".join(quoteList)
	_quote = cleanParagraph(_quote, removeQuotes = False)
	
	# Gets rid of awkwardly placed quotation marks
	_quote = _quote.replace('\"', "")
	_quote = r'"' + _quote + r'"'
	return _quote


# Obtains the first seen name in the tree
# NOTE that it will pick up only names that start capitalized
def getName(subtree):
	flat = subtree.flatten()
	posList = flat.leaves()
	names = [pair[0] for pair in posList if re.match('N.*', pair[1])]
	names = [name for name in names if re.match(r'[A-Z][a-z]+', name)] 
	if len(names) > 1:
		names = [name for name in names if name.lower() not in prefixes]
	return names[0] if len(names) > 0 else None

# Obtains the human readable version of the tree - the original document
def getParagraph(tree):
	flat = tree.flatten() # Returns a tree with leaves of (word, pos)
	posList = flat.leaves()	
	phrase = ""
	for pair in posList:
		phrase += pair[0] + " "
	return cleanParagraph(phrase)

# Returns a set of words containing the words in the tree
def getWordSet(tree):
	flat = tree.flatten()
	posList = flat.leaves()
	return set([pair[0] for pair in posList])



def cleanParagraph(phrase, removePeriod = False, removeQuotes = False):
	# TODO: Actually, the following are faster with simple string replace
	phrase = re.sub(r"\( ", r"(", phrase)
	phrase = re.sub(r" \)", r")", phrase)
	phrase = re.sub(r" ,", ",", phrase)
	phrase = re.sub(r"`` ", "\"", phrase)
	phrase = re.sub(r" ''", "\"", phrase)
	phrase = re.sub(r" \?", r"?", phrase)
	phrase = re.sub(r" n't", "n't", phrase)
	phrase = re.sub(r" \'s", "\'s", phrase)
	phrase = re.sub(r"- ", "-", phrase)
	phrase = re.sub(r" \.", ".", phrase)
	phrase = re.sub(r" ;", ";", phrase)
	phrase = re.sub(r" :", ":", phrase)
	phrase = re.sub(r" '", "'", phrase)
	phrase = re.sub(r" !", "!", phrase)
	phrase = re.sub(r" ''", "\"", phrase)

	if removePeriod:
		phrase = phrase.replace(".", "")
	if removeQuotes:
		phrase = phrase.replace("\"", "")
	return phrase.strip()
