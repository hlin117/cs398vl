#!/usr/bin/python
from paragraphscore import AdjScorer
from nltk.tokenize import word_tokenize
from colorama import *
import unicodedata
import os, json
from describe import Describer
"""
A script to process the text in the once and future king.

First, we're interested in describing the characteristics about
*certain* characters in the book. We would need to filter out the text 
that only pertain to our limited set of characters.

After, we would need to do the following:

1. Filtering out quoted passages that contain less than 60 (or so) words.
2. Filtering out unquoted passages that contain less than 100 words.
"""

gender = 'm'
book = "ofk-ch1"
char = "merlyn"
def main():
	paragraphObjs = getParagraphObjs(book)	
	paragraphObjs = filterByLength(paragraphObjs)
	passages = [obj for obj in paragraphObjs if obj["type"] == "passage"]
	quotes = [obj for obj in paragraphObjs if obj['type'] == "quote"]

	passages = sortPassages(passages)
	
	passages_pass = dict()
	passages_pass["character"] = char
	passages_pass["data"] = passages

	outputname = os.path.join("ofk", char + "-passages-" + book + ".json")
	with open(outputname, 'w') as outfile:
		json.dump(passages_pass, outfile, indent = 4, ensure_ascii = False)
		print Fore.GREEN + "Created file " + outputname + Fore.RESET

## I decided that I'm not going to show quotes anymore
#
#
#	quotes = sortQuotes(quotes)
#	quotes_pass = dict()
#	quotes_pass["character"] = char
#	quotes_pass["data"] = quotes
#
#	outputname = os.path.join("viz", char + "-quotes-" + book + ".json")
#	with open(outputname, 'w') as outfile:
#		json.dump(quotes_pass, outfile, indent = 4, ensure_ascii = False)
#		print Fore.GREEN + "Created file " + outputname + Fore.RESET
#


def sortPassages(passages):
	describe = Describer(book, char, gender)
	passages = describe.rank(passages)
	passages = [dict([("paragraph", obj['paragraph']), \
						('type', obj['type']), \
						('score', describe.score(obj['paragraph']))]) \
						for obj in passages]
	return passages


# Uses TF-IDF to rank each one of the sentences
def sortQuotes(quotes):
	
	describe = Describer(book, char, gender)
	quotes = describe.rank(quotes)
	quotes = [dict([("paragraph", obj['paragraph']), \
						('type', obj['type']), \
						('score', describe.score(obj['paragraph'], type_t = "quote"))]) \
						for obj in quotes]
	return quotes

#	print paragraphObjs
# Gets the paragraph object associated with the given book
def getParagraphObjs(book):
	filename = os.path.join("ofk", book + "-" + char + ".json")
	return json.load(open(filename))

# Filters the paragraph objects by length
def filterByLength(paragraphObjs, quoteThresh = 50, passageThresh = 100):
	return [obj for obj in paragraphObjs \
		if (obj["type"] == "quote" and len(word_tokenize(obj['paragraph'])) >= quoteThresh) \
		or (obj["type"] == "passage" and len(word_tokenize(obj['paragraph'])) >= passageThresh)]
	

if __name__ == "__main__":
	main()
