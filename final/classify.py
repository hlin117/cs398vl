# Implements a "bag of words" model to understand the semantic meaning of text.
from __future__ import division
from colorama import *
from nltk.corpus import wordnet as wn

class Classifier():

	# Constructor for the classifier. Input is a list of objects. Each object contains
	# 1. Words that describe the category, 2. A category name
	# The type parameter is what part of speech the tag is
	# TODO: I have no idea how to make this code more elegant
	def __init__(self, type, * data):
		self.setIdentifier(type)
		self.clusters = dict()
		for d in data:
			self.clusters[d["name"]] = set()

			# Need to figure out a good "synset" to use for the words... 
			# Use the first one, I guess... 
			for word in d["data"]:
				synsets = wn.synsets(word)	
				for synset in synsets:
					if synset.pos in self.tag:
						self.clusters[d["name"]].add(synset)	
						break
		
	# Sets the identifiers, depending on the part of speech type given
	def setIdentifier(self, type):
		if type == "noun":
			self.tag = "n"
		elif type == "adj":
			self.tag = "a" # s for adjective satellites
		elif type == "verb":
			self.tag = "v"
		else:
			print Fore.RED + "WARNING: Unrecognized part of speech." + Fore.RESET

	# Classifies a word based upon the current synsets in the clusters dictionary
	# Just performs a max of each word against each cluster
	def classify(self, word, num = False):
		if not num:
			return max(self.clusters.items(), key = lambda item : self.score(word, item[0]))[0]	

		# TODO: Maybe find a better way to calculate the running time for this...
		else:
			best = max(self.clusters.items(), key = lambda item : self.score(word, item[0]))[0]		
			return (best, self.score(word, best))

			

	# Calculates the similarity between the given word and each synset given.
	# When normalize is true, we take into account the size of the considered words.
	def score(self, word, key, normalize = True):
		word_synsets = wn.synsets(word)
		word_synsets = [synset for synset in word_synsets if synset.pos == self.tag]

		# Considers all of the scores from each one of the synsets
		# TODO: Maybe compact this into one function?
		similarity = 0
		for given_synset in word_synsets:
			tempSimilarity = self.clusterScore(given_synset, key)
			if tempSimilarity > similarity:
				similarity = tempSimilarity
		
		return similarity
	
	# Gets the score of the cluster, given the synset
	# TODO: Length normalization might be too harsh here
	def clusterScore(self, given_synset, key):
		cluster = self.clusters[key]
		tempSimilarity = 0
		for synset in cluster:
			score = synset.path_similarity(given_synset)
			tempSimilarity += float(score)
		return tempSimilarity / float(len(cluster))
	
	# A neat wrapper around classify that has a print function
	def result(self, word, num = True):
		(classification, score) = self.classify(word, num = True)
		print word + " is classified as " + classification + " with score " + str(score)
		
