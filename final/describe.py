from __future__ import division
from parsetrees import *
from nltk.stem.porter import PorterStemmer
from nltk.tokenize import word_tokenize
from nltk.probability import FreqDist
import nltk
from os.path import join
from math import log
from parser import getParsedParagraphs 
class Describer(FreqDist):

	# Populates the FreqDist with adjectives!
	# Fills the histogram/freqdist with adjectives used throughout the book
	def __init__(self, book, character, gender = 'm'):
		super(Describer, self).__init__()
		paragraphs = getParsedParagraphs(book) 
		self.stemmer = PorterStemmer()
		self.avgLen = None

		# Sorry, can't do this in one line =(
		for para in paragraphs:
			for pair in para:
				if pair[1] == "JJ":
					self.inc(self.stemmer.stem(pair[0]))
		self.char = character.lower()
		self.gender = gender.lower()
		if self.gender == 'm':
			self.pronouns = set(['he', 'his', 'him'])
		else:
			self.pronouns = set(['her', 'she'])

	# Ranks the dataset
	def rank(self, paragraphObjs):
		# Need to find the average length of a document in the dataset,
		# then score each document
		if self.avgLen is None:
			totalLen = 0
			for obj in paragraphObjs:
				totalLen += len(word_tokenize(obj["paragraph"]))
			self.avgLen = totalLen / len(paragraphObjs)
		return sorted(paragraphObjs, key = lambda obj : self.score(obj["paragraph"], obj["type"]), reverse = True)


	# Scores how a passages "describes" a character in the text
	def score(self, paragraph, type_t = "passage"):
		if type_t == "quote":
			return self.scoreQuote(paragraph)
		else:
			return self.scorePassage(paragraph)

	# TODO: Probably not gonna finish this
	def scoreQuote(self, paragraph):
		return -10

	# Multiplies the number of pronouns seen in the passage,
	# times the score of each adjective in the passage. 
	# It also normalizes based upon passage length
	def scorePassage(self, paragraph):
		tok_para = word_tokenize(paragraph)
		pos_List = nltk.pos_tag(tok_para)

		# Trying to get rid of more than one subject in the paragraph
		numIrrelevantChars = 0
		tree = getParaTree(pos_List)
		for subtree in tree.subtrees(filter = lambda tree : tree.node == verbNoun):
			name = getName(subtree)
			if name is None:
				numIrrelevantChars += 1
			else:
			 	numIrrelevantChars += int(getName(subtree).lower() != self.char)

		# Heuristic!
		if numIrrelevantChars > 4:
			return 0

		# Putting a numerical score
		totalAdjScore = 0
		totalPronounScore = 0
		for pair in pos_List:

			# NOTE: This is a heuristic; there are probably passages out there
			# that have the word "they" in them
			if pair[0] == "they":
				return 0
			if self.isAdj(pair):
				totalAdjScore += self.adjScore(pair[0])
			elif self.isPronoun(pair):
				totalPronounScore += self.pronounScore(pair[0])


		return totalAdjScore * totalPronounScore * (self.avgLen / len(tok_para))

	def isAdj(self, pair):
		return pair[1] == "JJ"
	
	def isPronoun(self, pair):
		return pair[1] == "PRP" or pair[1] == "PRP$"

	# Note that this number is probably going to be very small...
	def adjScore(self, adj):
		return self.freq(self.stemmer.stem(adj))
	
	def pronounScore(self, pronoun):
		return int(pronoun in self.pronouns)


	

