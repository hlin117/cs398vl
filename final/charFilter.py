#!/usr/bin/python
import os, re
from parsetrees import *
from colorama import *
from parser import *

CHAR = "lancelot".lower()
FILE = "ofk-ch3"

"""
The purpose of this script is to extrapolate important sentences about 
a given character.
"""
def main():
	paragraphs = getParsedParagraphs(FILE)
	paraTrees = getParagraphTrees(paragraphs)
	relParagraphs = getRelevantParagraphs(paraTrees)

	output = os.path.join("ofk", FILE + "-" + CHAR + ".json")
	with open(output, 'w') as file:
		json.dump(relParagraphs, file, ensure_ascii = False, indent = 4)

"""
Goes throught the paragraph trees to see whether it can scrape paassages
that refer to the character of interest
"""
def getRelevantParagraphs(paraTrees):

	output = list()

	# I built a finite state machine for this.... I hope it actually works though =\
	speaker1 = None # The person who just spoke
	speaker2 = None # The person who spoke two quotes ago
	type_t = None
	for tree in paraTrees: 

		if hasQuote(tree):
			subject = analyzeQuote(tree, speaker1, speaker2)
			paragraph = getParagraph(tree)
			type_t = "quote"

			speaker2 = speaker1
			speaker1 = subject
		else:
			subject = analyzePassage(tree, speaker1, speaker2)
			paragraph = getParagraph(tree)
			type_t = "passage"

			# If there is no subject in the next paragraph, and it's just transition, reset the state
			if subject is None:
				speaker1 = None 
				speaker2 = None
			else:
				speaker2 = speaker1
				speaker1 = subject

		if subject is None:
			continue

		# Need to decide whether to keep the sentence
		if paragraph is not None and subject.lower() == CHAR:
			output.append({"type": type_t, "paragraph": paragraph})
		type_t = None
	return output

# Analyze who is speaking in the quote
# Returns the "subject" of the passage
def analyzeQuote(tree, speaker1, speaker2):
	if hasSpeaker(tree):
		return getSpeaker(tree)
	return speaker2 if speaker2 is not None else speaker1

# Analyzes who is the subject of the passage
# Returns the "subject" of the passage
def analyzePassage(tree, speaker1, speaker2):
	if startsWithPronoun(tree):
		return speaker1
	if startsWithName(tree):
		return getFirstName(tree)

	nameArr = getNameArray(tree)
	nameCount = dict()
	for name in nameArr:
		nameCount[name] = nameCount.get(name, 0) + 1
	return None if len(nameArr) == 0 else max(nameCount.iteritems(), key = lambda kvPair : kvPair[1])[0]	
			


if __name__ == "__main__":
	main()
