import nltk, json, unicodedata
import os, re
from colorama import *

# First need to split our text into proper paragraphs,
# then save this "cleaned" file onto disk
def getParsedParagraphs(book):

	# If a cached file exists on disk, just use it
	cleaned = os.path.join("ofk", book + ".cleaned")
	if os.path.exists(cleaned):
		print Fore.GREEN + "Reading cached file... " + Fore.RESET
		paragraphs = json.load(open(cleaned))
		outputlist = []
		for para in paragraphs:
			sublist = []
			for pair in para:
				word = unicodedata.normalize("NFKD", pair[0]).encode('ascii', 'ignore')
				tag = unicodedata.normalize("NFKD", pair[1]).encode('ascii', 'ignore')
				sublist.append((word, tag))
			outputlist.append(sublist)

		return outputlist

	# Otherwise we have to make one
	else:
		textpath = os.path.join("ofk", book + ".txt")
		text = str(open(textpath).read())
		text = fixParagraphBreaks(text)
		text = separateQuotes(text)

		# Parsing paragraphs
		paragraphs = text.split("\n\n")
		output = []
		for para in paragraphs:
			para = prepareParagraph(para)
			pos_list = tagParagraph(para)
			output.append(pos_list)

		with open(cleaned, "w") as outfile:
			print Fore.GREEN + "Writing to file " + cleaned + Fore.RESET
			json.dump(output, outfile, indent = 4, ensure_ascii = False)

		return output

# A list of modifications we do to the paragraph that makes POS results look better
def prepareParagraph(para):
	para = para.replace(".", " .") # Adding spaces to periods
	para = para.replace("\n", " ")			# Removing annoying endlines
	para = re.sub(r"n'[^a-z]", "ng ", para) 	# Removing differences in English
	return para

# Performs tokenization, and tags the paragraph
def tagParagraph(para):
	para = nltk.tokenize.word_tokenize(para)
	pos_list = nltk.pos_tag(para)
	return pos_list

# If we see the string r'" "', this means that there is more than
# one quote in a paragraph. We then create another paragraph
def separateQuotes(text):
	return text.replace(r'" "', '\"\n\n\"')
	

# The idea is that sometimes, there are awkward paragraph breaks.
# This function removes these awkward paragraph breaks between sentences.
# TODO: I'm not sure how to refractor this. Any suggestions would be nice
def fixParagraphBreaks(text):	
	
	newText = ""
	total = 0
	count = 0
	for i in range(0, len(text)):
		if text[i] == '\n':
			count += 1

			# Case when we need to check whether next character is lowercased
			if count == 2:
				count = 0
				try:
					if text[i+1].isalpha() and text[i+1] == text[i+1].lower():
						total += 1
						continue
				except:
					continue
		elif count == 1:
			count = 0
		newText += text[i]

	return newText
