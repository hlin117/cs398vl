#!/usr/bin/python
import json
from colorama import *
from os.path import join
from paragraphscore import AdjScorer

"""
Decorates the paragraph by bolding significant parts of the sentences
"""
gender = 'm'
book = "ofk-ch1"
char = "merlyn"
limit = 5
def main():


	jsonData = json.load(open(join("ofk", char + "-passages-" + book + ".json")))
	passages = jsonData["data"]

	scorer = AdjScorer(book)
	passages = [{"type": obj["type"], 
				"score": obj["score"],
				"paragraph": scorer.decorateParagraph(obj["paragraph"], limit = 5)}
				for obj in passages]

	passages_pass = dict()
	passages_pass["character"] = char
	passages_pass["data"] = passages

	outputname = join("viz", char + "-passages-" + book + ".json")
	with open(outputname, "w") as outfile:
		json.dump(passages_pass, outfile, indent = 4, ensure_ascii = False)
		print Fore.GREEN + "Created file " + outputname + Fore.RESET

if __name__ == "__main__":
	main()

