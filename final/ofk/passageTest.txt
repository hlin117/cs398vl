In the castle of Benwick, the French boy was looking at his face in the polished surface of a
kettle-hat . It flashed in the sunlight with the stubborn gleam of metal . It was practically the
same as the steel helmet which soldiers still wear, and it did not make a good mirror, but it
was the best he could get. He turned the hat in various directions, hoping to get an average
idea of his face from the different distortions which the bulges made . He was trying to find
out what he was, and he was afraid of what he would find .

The boy thought that there was something wrong with him. All through his life-- even when
he was a great man with the world at his feet--he was to feel this gap: something at the
bottom of his heart of which he was aware, and ashamed, but which he did not understand.
There is no need for us to try to understand it. We do not have to dabble in a place which he
preferred to keep secret.

The Armoury, where the boy stood, was lined with weapons of war. For the last two hours he
had been whirling a pair of dumb-bells in the air--he called them "poises"--and singing to
himself a song with no words and no tune. He was fifteen. He had just come back from
England, where his father King Ban of Benwick had been helping the English King to quell a
rebellion. You remember that Arthur wanted to catch his knights young, to train them for the
Round Table, and that he had noticed Lancelot at the feast, because he was winning most of
the games.

