from __future__ import division
import re
from parser import *
from parsetrees import *
from nltk.stem.porter import PorterStemmer
from nltk.tokenize import word_tokenize, sent_tokenize
from nltk.probability import FreqDist
import nltk

# Scores each adjective in a given paragraph.
class AdjScorer(FreqDist):

	# Populates the freqdist with adjectives from the book.
	# This is similar to what the Describer object does.
	def __init__(self, book):
		super(AdjScorer, self).__init__()
		paragraphs = getParsedParagraphs(book)
		self.stemmer = PorterStemmer()
	
		for para in paragraphs:
			for pair in para:
				if "JJ" in pair[1]:	# This is actually different than describe.py
					self.inc(self.stemmer.stem(pair[0]))
		
	# Takes in an adjective, and scores the adjective
	# Scores the adjective based upon its frequency throughout the whole corpus.
	# Returns the score of the adjective
	# This is the same function as in describe.py
	def adjScore(self, adj):
		return self.freq(self.stemmer.stem(adj))
		
	# Decorates the paragraph by 
	# 1. First finding the top number of "rare" adjectives in the paragraph
	# 2. Creating the parse tree of the paragraph
	# 3. Filtering the "appropriate pos" of that chunk and its content, put it into a set
	# 4. Run through the original paragraph again, putting <b> tags on those chunks
	def decorateParagraph(self, paragraph, limit = 5):
		processed = prepareParagraph(paragraph)
		words = word_tokenize(processed)
		pos_words = nltk.pos_tag(words)

		# Get the top ranked adjectives in the paragraph
		adjList = [(pair[0], self.adjScore(pair[0])) for pair in pos_words if "JJ" in pair[1]]
		adjList = sorted(adjList, key = lambda pos_pair : pos_pair[1], reverse = True)

		adjSet = set()
		try:
			for i in xrange(0, limit):
				adjSet.add(adjList[i][0])
		except:
			pass

		# Creating the parse tree of the paragraph
		tree = getParaTree(pos_words, grammar = True)
		
		# Finding good subtrees with the adjective
		phrases = getPhrases(tree, "adj")	
		phrases = [getParagraph(subtree) for subtree in phrases if len(adjSet.intersection(getWordSet(subtree))) != 0]


		for phrase in phrases:
			paragraph = paragraph.replace(phrase, "<b>" + phrase + "</b>")

		return paragraph

