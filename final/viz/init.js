var limit = 1;
var state = "lancelot";

$(document).ready(function() {
	var lancelot = "lancelot-passages-ofk-ch3.json";
	var merlyn = "merlyn-passages-ofk-ch1.json";
	var arthur = "arthur-passages-ofk-ch2.json";

	$("#selection").append("<li class='item' id='lancelot-btn'><a href='#'>Lancelot</a></li>");
	$("#selection").append("<li class='item' id='merlyn-btn'><a href='#'>Merlyn</a></li>");
	$("#selection").append("<li class='item' id='arthur-btn'><a href='#'>Arthur</a></li>");

	insertData(lancelot);


	$(".item").click(function() {
		character = this.id.split("-")[0]
		if(character == "lancelot" && state != "lancelot") {
			$("#text").html("");
			insertData(lancelot);
			state = "lancelot";
		}
		else if(character == "merlyn" && state != "merlyn") {
			$("#text").html("");
			insertData(merlyn);
			state = "merlyn";
		}

		else if(character == "arthur" && state != "arthur") {
			$("#text").html("");
			insertData(arthur);
			state = "arthur";
		}
		

	});

});

function insertData(jsonPath) {
	$.getJSON(jsonPath, function(jsonData) {
		var output = new Array();
		for(var i = 0; i < limit; i++) {
			if(jsonData.data[i].score != 0)
				output.push(jsonData.data[i]);
			else break;
		}

		$("#text").append("<h1>About " + jsonData.character + "...</h1>");
		for(var i = 0; i < output.length; i++)
			$("#text").append("<span>" + output[i].paragraph + "</span><br><br>");

		
	});


}

