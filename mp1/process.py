__author__ = 'hlin117'

from colorama import *
import re, nltk
from nltk.corpus import stopwords

"""
Defines a procedure to clean up the text. This is the
hardest part of MP1.

Returns a modified list
"""
def process(corpusList):
	corpusList = lowercase(corpusList)
	corpusList = removePunct(corpusList)
	corpusList = removeApostrophe(corpusList)
	return corpusList

def lowercase(corpusList):
	return [word.lower() for word in corpusList]


def removeApostrophe(corpusList):
	return [re.sub("'s", "", word) for word in corpusList]

def stdCleaner(corpusList):
	corpusList = removePunct(corpusList)
	corpusList = onlyWords(corpusList)
	corpusList = stem(corpusList)
	corpusList = removeStop(corpusList)
	return corpusList 


"""
<----------------------------------------------------------------------->
"""


def removePunct(corpusList):
	return [re.sub('\.', '', word).strip() for word in corpusList]

#only keep alphabetic strings
def onlyWords(corpusList):
	return [w for w in corpusList if w.isalpha()] 
	

def stem(corpusList):
	return [nltk.PorterStemmer().stem(word) for word in corpusList]

def removeStop(corpusList):
	stopwords_set = set(stopwords.words("english"))
	return [w for w in corpusList if w not in stopwords_set]


