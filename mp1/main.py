#!/usr/bin/python

__author__ = 'hlin117'

import nltk, json, sys
from sys import argv
from colorama import *
from process import process

global book1, book2, book3, book4, orkney
book1 = ["wart", "merlyn", "kay", "ector", "authur"]
book2 = ["arthur", "merlyn", "kay", "morgause"]
book3 = ["arthur", "lancelot", "guenever", "galahad", "elaine", "mordred"]
book4 = ["arthur", "lancelot", "mordred", "guenever"]
orkney = ["gawaine", "gaheris", "gareth", "agravaine"]


"""
Counts the number of occurances of characters in the story of
"The Once and Future King". 

Takes in the parameter of which text file number. Assumes that
the files have been appropriately preprocessed.
"""
def main():

	(filename, pp, output, wordRate, folder, book) = getArgs()

	nameSet = getNames()
	processed = processText(filename, pp)
	listDict = getListDict(processed, nameSet, wordRate)
	createFile(listDict, output, wordRate, folder, nameSet, book)

# Obtains the command line arguments
def getArgs():
	wordRate = 350
	pp = True
	folder = "website"
	output = "ofk"
	bookList = ["ofk.txt", "ofk-ch1.txt", 'ofk-ch2.txt', 'ofk-ch3.txt', 'ofk-ch4.txt']

	# TODO: For some odd reason, I can't accept integers as command line arguments.
	# Regardless, one would be able to modify which book they'd like to run
	try:
		book = atoi(argv[1])
		print book
		filename = "ofk/" + bookList[book]
	except:
		book = 0
		print str(book)
		filename = "ofk/" + bookList[0]
	print "Using " + filename

	return (filename, pp, output, wordRate, folder, book)

# Processes the text, returns a corpusList that has been processed
def processText(filename, pp):
	raw_text = open(filename).read()                   # Opens the file
	tokens_raw = nltk.word_tokenize(raw_text)
	return process(tokens_raw) if pp else tokens_raw   # Processes the list of words, returns a list

# Creates the set of names of interest
def getNames():

	output = set()
	output.update(book1)
	output.update(book2)
	output.update(book3)
	output.update(book4)
	output.update(orkney)
	return output

# Input is a list of words that have been processed.
# Returns a list of word-frequency pairs. Each index refers
# to a word-freq pair for each wordRate words. 
# Returns a long list of dictionaries
def getListDict(processed, nameSet, wordRate):
	outputList = []
	for i in range(0, len(processed)):
		if i % wordRate == 0 and i != 0:
			outputList.append(countNames(processed, nameSet, i-wordRate, i-1))
	
	remainder = i % wordRate
	outputList.append(countNames(processed, nameSet, i - remainder, i))

	return outputList


# Counts the occurance of each name in the nameSet inside the text.
# Returns a dictionary of name - frequency pairs
def countNames(text, nameSet, start, end):

	# Add items to the dictionary. Note we're double counting
	# for pronouns
	outputDict = {}
	for i in range(start, end):

		# Add +1 default
		if text[i] in nameSet:
			outputDict[text[i]] = outputDict.get(text[i], 0) + 1
	
	return outputDict

"""
Creates a JSON object that is a list of dictionaries
listDict is a list of dictionaries, where each index refers to each numWord pages.
Each dictionary consists of name - freq pairs.
"""
def createFile(listDict, output, wordRate, folder, nameSeti, book):

	# All of the names possible
	nameSet = set()
	if book == 0:
		nameSet.update(book1)
		nameSet.update(book2)
		nameSet.update(book3)
		nameSet.update(book4)
		nameSet.update(orkney)

	elif book == 1:
		nameSet.update(book1)
	elif book == 2:
		nameSet.update(book2)
		nameSet.update(orkney)
	elif book == 3:
		nameSet.update(book3)
		nameSet.update(orkney)
	else:
		nameSet.update(book4)
		nameSet.update(orkney)

	numNames = len(nameSet)

	# Creating the dictionary of names, so we could associate a name with an index
	i = 0; nameDict = {}
	for name in nameSet:
		nameDict[name] = i; i += 1

	# Set up an outputList that has numNames lists
	outputList = [[] for name in nameSet]

	# Now iterate though the dictionaries in listDict, and
	# continuouly append to outputList. Make sure to
	# take into account of dictionaries that do not have names.
	for i in range(len(listDict)):
		for name in nameSet:
			index = nameDict[name]
			obj = {}
			obj["x"] = i
			obj["y"] = listDict[i].get(name, 0)
			outputList[index].append(obj)

	outputName = folder + "/" + output + "-" + str(wordRate) + ".js"
	with open(outputName, 'w') as outfile:
		outfile.write("data = ");
		outfile.write(json.dumps(outputList, ensure_ascii = False) + ";\n\n");
		outfile.write("nameArray = new Array();\n")

		for name in nameSet:
			index = nameDict[name]
			outfile.write("nameArray[" + str(index) + "] = new Object();\n")
			outfile.write("nameArray[" + str(index) + "].name = '" + name + "';\n")
			outfile.write("nameArray[" + str(index) + "].color = null;\n\n")
		print "Wrote the file " + outputName

if __name__ == "__main__":
	main()
