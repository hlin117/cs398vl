
$(document).ready(function() {

	// Draws the graph with the drawlist
	drawNewGraph();
});

// Used when we're switching books. We have to reinitialize everything
function drawNewGraph() {
	initialize();
	drawGraph();
}

// ONLY CALL THIS FUNCTION WHEN ENTERING THE PAGE. This initializes the
// drawList, something we can't do in globalVars
function initialize() {
	numPages = data[0].length;
	numChar = data.length;

	for(var i = 0; i < numChar; i++)
		drawList[i] = true;
}

// Given the input of which characters should be drawn, appropriately 
// determins the data of which to use
function drawGraph() {

	$("#svg-placeholder").html("");	// Clears current contents
	var outputObj = getData();
	createSVG(outputObj);
}

// Given an array of booleans that specifies which characters 
// are to be drawn on the screen, returns the data corresponding to the data
function getData() {

	dataArray = new Array();
	mapping = new Array();		// Maps the values of the outputArray
								// to nameArray, where data is stored
	
	for(var i = 0; i < drawList.length; i++) {
		if(drawList[i]) {
			dataArray.push(data[i]);
			mapping[dataArray.length-1] = i;
		}
	}
	
	numPages = dataArray[0].length;
	numChar = dataArray.length;

	output = new Object();
	output.dataArray = dataArray;
	output.mapping = mapping;
	return output;
}




// Contains all of the logic needed to create the graph.
function createSVG(outputObj) {

	var data = outputObj.dataArray
	var n = numChar;
	var m = numPages;
	console.log("n is " + numChar + ", m is " + numPages);
	var numWords = 350;
	var stack = d3.layout.stack();

	/**
	 * This is the part of the function that we're modifying
	 * Note that each item represents *A DATA IN THE STACK, NOT A SINGLE STACK ITSELF*
	 * 
	 * Therefore, the number of items in a set represents the number of samples per layer, and
	 * the number of sets represents the number of layers.
	 */
	var layers = stack(data);

	// Max height of the bars
	var yGroupMax = d3.max(layers, function(layer) { return d3.max(layer, function(d) { return d.y; }); }),
	yStackMax = d3.max(layers, function(layer) { return d3.max(layer, function(d) { return d.y0 + d.y; }); });

	// Styling
	var margin = {top: 40, right: 10, bottom: 20, left: 15},
		width = 1000 - margin.left - margin.right,
		height = 600 - margin.top - margin.bottom;

	// x scale
	var x = d3.scale.ordinal()
		.domain(d3.range(m))		// Describe the number of layers
		.rangeRoundBands([0, width], .08);

	// y scale
	var y = d3.scale.linear()
		.domain([0, yStackMax])
		.range([height, 0]);


	// Colors the graphs on the screen
	var color = d3.scale.ordinal()
		.domain([0, n - 1])
		.range(colorArray);


	var xTicks = d3.scale.ordinal()
		.domain(d3.range(m).map(function(x) { return x }))		// Describe the number of layers
		.rangeRoundBands([0, width], .08);

	var axisScale = d3.scale.linear()
		.domain([0, numPages])
		.range([0, numPages*numWords-900]);

	var xAxis = d3.svg.axis()
		.scale(axisScale)
		.orient("bottom");	
		
	// Create the y scale
	var yAxis = d3.svg.axis().scale(y)
		.orient("left");

		
	// Puts the svg object on the screen, and gives the following html attributes
	var svg = d3.select("#svg-placeholder").append("svg")
		.attr("width", 900)/*width + margin.left + margin.right)*/
		.attr("height", height + margin.top + margin.bottom)
		.append("g")
		.attr("transform", "translate(" + margin.left + "," + margin.top + ")");

	// classed adds a class value, attr will overwrite
	svg.append("g").attr("class", "axis").attr("id","y-axis").call(yAxis);

	// Puts the data inside the svg
	var layer = svg.selectAll(".layer")
		.data(layers)
		.enter().append("g")
		.attr("class", "layer")
		.style("fill", function(d, i) { return nameArray[mapping[i]].color; });

	var rect = layer.selectAll("rect")
		.data(function(d) { return d; })
		.enter().append("rect")
		.attr("x", function(d) { return x(d.x); })
		.attr("y", height)
		.attr("width", x.rangeBand())
		.attr("height", 0);

	rect.transition()
		.delay(function(d, i) { return i * 10; })
		.attr("y", function(d) { return y(d.y0 + d.y); })
		.attr("height", function(d) { return y(d.y0) - y(d.y0 + d.y); });

	// The code for the x axis
	svg.append("g")
		.attr("class", "x axis")
		.attr("transform", "translate(0," + height + ")")
		.call(xAxis);

	// For the title
	svg.append("text")
		.attr("x", (width / 2))
		.attr("y", 0 - (margin.top / 2))
		.attr("text-anchor", "middle")
		.style("font-size", "16px")
		.style("text-decoration", "underline")
		.text("The Once and Future King");
		
	d3.selectAll("input").on("change", change);

	function transitionGrouped() {
		y.domain([0, yGroupMax]);
		
		// Create the y scale
		yAxis = d3.svg.axis().scale(y)
			.orient("left");
		
		// classed adds a class value, attr will overwrite
		//	svg.append("g").attr("class", "axis").call(yAxis);
		d3.select("#y-axis").call(yAxis);


		rect.transition()
			.duration(500)
			.delay(function(d, i) { return i * 10; })
			.attr("x", function(d, i, j) { return x(d.x) + x.rangeBand() / n * j; })
			.attr("width", x.rangeBand() / n)
			.transition()
			.attr("y", function(d) { return y(d.y); })
			.attr("height", function(d) { return height - y(d.y); });
	}

	function transitionStacked() {
		y.domain([0, yStackMax]);

		d3.select("#y-axis").call(yAxis);
		rect.transition()
			.duration(500)
			.delay(function(d, i) { return i * 10; })
			.attr("y", function(d) { return y(d.y0 + d.y); })
			.attr("height", function(d) { return y(d.y0) - y(d.y0 + d.y); })
			.transition()
			.attr("x", function(d) { return x(d.x); })
			.attr("width", x.rangeBand());
	}

	function change() {
		var timeout = setTimeout(function() {
		  d3.select("input[value=\"grouped\"]").property("checked", true).each(change);
		}, 1000);
		
		clearTimeout(timeout);
		if (this.value === "grouped") transitionGrouped();
		else transitionStacked();
	}



}


