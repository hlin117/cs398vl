// nameArray[index] is officially initialized in the bigData file
// I would otherwise put the "person" objects in the globalVar file
for(var i = 0; i < nameArray.length; i++)
	nameArray[i].color = colorArray[i]

// nameArray is declared in globalVars
for(var i = 0; i < nameArray.length; i++) {

	jQuery('<a>', {
		text: capitalize(nameArray[i].name),
		class: "list-group-item",
		href: "#",
		id: "name-" + i,
		style: "color: white; background-color: " + nameArray[i].color
	}).data("index", i).appendTo('#name-body');

	$("#name-" + i).on("click", function() {
		var index = $(this).data("index");

		// If the name is clicked once, then make the name black
		if(drawList[index]) $(this).css("color", "black");

		// Toggle the color back
		else $(this).css("color", "white");
		reDraw(index);
	});
}

// Redraws the graph without index i and other items marked
function reDraw(i) {

	drawList[i] = drawList[i] ? false : true;	
	drawGraph();
}


function capitalize(string) {
	return string.charAt(0).toUpperCase() + string.slice(1);
}
