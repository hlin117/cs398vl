README FOR MP1 OF CS398VL

This project obtains the frequencies of names throughout The Once and Future King. It's supposed to be similar to what the Lord of the Rings project did with their name mentions:
http://lotrproject.com/statistics/books/charactermentions

The current finshed project can be viewed at "web.engr.illinois.edu/~halin2". When you click on the "stacked" button, each one of the bars represents the summed frequencies of the names shown on the right per 350 words. The y-axis shows the summed frequencies in the stacked mode.

Likewise, you could switch between books. Each one of the books has a corresponding set of names.

You can click on the names to the right in case you would like to "hide" one of the names. 


CURRENT BUGS

Sorry, I have problems with the interface, I'm not an expert d3-er yet. The x-axis ticks aren't showing up, even though Cole helped me get them to work. Also, when you enter the webpage, the initial radio button isn't checked.

IMPROVEMENTS?

It would be cooler if you could zoom into the image, and widen the bars. But... I'm not a d3 expert yet. I could probably improve the interface too. 