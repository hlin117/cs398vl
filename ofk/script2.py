#!/usr/bin/python

import nltk, json

"""
We now have a json file that has paragraphs filled with sentences.

Objective of this file is to understand who is speaking in a given conversation. 
Need a way to tag a person to a quoted sentence.
Need a way to partition these sentences into conversations.

First job is to use ne_relations on each one of these sentences.
"""

paragraphs = json.load(open("ofk-ch1.para"))

paragraphs = [[nltk.word_tokenize(sent) for sent in para] for para in paragraphs]
paragraphs = [[nltk.pos_tag(sent) for sent in para] for para in paragraphs]

print paragraphs

"""
for para in paragraphs:
	try:
		output.append(nltk.pos_tag(sent) for sent in para)
	except:
		print "couldn't parse: " + sent 
print output
"""
with open("ofk-ch1.para.pos", "w") as outfile:
	json.dump(paragraphs, outfile, indent = 4)
	
