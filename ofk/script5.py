#!/usr/bin/python

import json, nltk
			
class wtpair():

	def __init__(self, pair):
		self.pair = pair

	def pos(self):
		return pair[1]
	
	def word(self):
		return pair[0]

class quote():

	def __init__(self, person, quote):
		self.person = person
		self.quote = quote

"""
Splits the story into a list, delimited by the \n\n character.
Afterwards, create a list of "quote" objects that are: (speaker, quote)
"""
book = "ofk-ch1"
"""
text = open(book + ".txt").read()

paragraphs = text.split("\n\n")
paragraphs = [nltk.word_tokenize(para) for para in paragraphs]
paragraphs = [nltk.pos_tag(para) for para in paragraphs] 
with open(book + ".para2.pos", "w") as outfile:
	json.dump(paragraphs, outfile, indent = 4, ensure_ascii = False)
	"""

paragraphs = open(book + ".para2.pos").read()
paragraphs = [[wtpair(pair) for pair in para] for para in paragraphs]

"""
Now we find the quotes in our book. Think of our reader as a finite state machine.

It has three states:
	1. Scanning non-dialogue text
	2. Scanning quoted text
	3. Identifying the reader
"""

outputlist = []

for para in paragraphs:
	state = 1;	

	# para is a list of wtpair objects. We need to make sense out of it.	
	index = 0;
	while index < len(para):
			

