#!/usr/bin/python
from colorama import *
import json
import re
import nltk
from nltk import Tree
text = str(open("ofk-ch1.txt").read())
paragraphs = text.split("\n\n")
grammar = "QUOTE: {<``><.*>+?<''>}"
nPhrase = "nPHRASE: {<NNP>+}"
cp = nltk.RegexpParser(grammar)
nParser = nltk.RegexpParser(nPhrase)

objects = []
for para in paragraphs[0:10]:
	output = para.replace("\n", " ")
	output = re.sub(r"n'[^a-z]", "ng", output)
	output = nltk.word_tokenize(output)
	output = nltk.pos_tag(output)
	objects.append(output)

paragraphs = objects
objects = []

for para in paragraphs[0:10]:
	output = cp.parse(output)
	output = nParser.parse(output)
	objects.append(output)	

for T in objects:
	for subtree in T.subtrees(filter = lambda x : x.node == "QUOTE" or x.node == "nPHRASE"):
		print subtree 



