import json, nltk
"""
Now we have a file called text.para.pos, which represents:

a list of paragraphs
which each paragraph containing a list of words
and each "word" consisting of a 2-tuple (word, pos), which is a list in JSON

We need to convert these sentence lists into a single paragraph again for chunking.

"""

paragraphs = json.load(open("ofk-ch1.para.pos"))




paragraphs = [[[[(word, pos) for word, pos in sent] for sent in sentences] for sentences in para] for para in paragraphs]


