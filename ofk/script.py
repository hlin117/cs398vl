#!/usr/bin/python
import nltk, json
"""
text = open("ofk.txt").read()
sentences = [sent.replace('\n', ' ').strip() for sent in nltk.sent_tokenize(text)]
with open("ofk.sent", "w") as outfile:
	json.dump(sentences, outfile, ensure_ascii = False, indent = 4) 
"""
text = open("ofk-ch1.txt").read()
paragraphs = [para.strip() for para in text.split("\n\n")]
paragraphs = [nltk.sent_tokenize(para.replace("\n", " ")) for para in paragraphs] 

"""
paragraphs is now a python list of paragraphs.
Each paragraph is a list of sentences. Each sentence is a string.

Task now is to throw away paragraphs that do not add to the dialogue 

"""

output = []
for i in range(0, len(paragraphs)):
	try:
		if ('\"' in paragraphs[i][0]) or ('\"' in paragraphs[i+1][0]):
				output.append(paragraphs[i])
	except:
		print "finishing"

with open("ofk-ch1.para", "w") as outfile:
	json.dump(output, outfile, ensure_ascii = False, indent = 4) 
