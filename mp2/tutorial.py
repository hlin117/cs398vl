# Creating a synset for dog and cat
dog = wn.synsets('dog')[0]
cat = wn.synsets('cat')[0]

# Doing path similarity
dog.path_similarity(cat) # returns 0.2
# In general, two objects are similar if they are between 0.1 and 0.2 from each other

# More hypernym stuff
dog.lowest_common_hypernyms(cat) # NOTICE THE S. Returns a set of synsets

table = wn.synsets('table')[0]
dog.lowest_common_hypernyms(table) # Returns [('entity.n.01')]

# Other functions:
# synset.hypernym_paths()
# synset.common_hypsernyms(other)
# synset.lowest_common_hypernyms(other)
# synset.hypernym_distances()



