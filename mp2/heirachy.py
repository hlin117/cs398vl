__author__ = 'hlin117'

from nltk.corpus import wordnet as wn
import json

class Node():

	# Default constructor for the class
	def __init__(self):
		pass

	def __init__(self, name, size = 0):
		try:
			self.name = name
			self.size = size
			self.synset = wn.synsets(name, pos=wn.NOUN)[0]

		except:
			print "Trouble with: " + name
	def __repr__(self):
		output = "{0}: {1}".format(self.name, self.size)
		return output

class JsonNode():

	def __init__(self, node = None, name = "", children = None):
		# Case 1: The node is a leaf. Only node is passed in.
		if node is not None:
			self.name = node.name
			self.size = node.size
			self.children = None
		# Case 2, the node is a parent. The JsonNode's name,
		# and list of children are passed in
		else:
			self.name = name
			self.children = children
			self.size = 0
			for child in children:
				self.size += child.size

	# Will recurisvely create an object representation of itself
	def objectRep(self):
		output = dict()
		output["name"] = self.name
		output["size"] = self.size

		# Recurse on the children, if any
		if self.children is not None:
			output["children"] = [child.objectRep() for child in self.children]
		return output

	def __repr__(self):
		if self.children is not None:
			output = "'name': {0},\n'size': {1}\n'children': {2}".format(self.name, self.size, self.children)
		else:
			output = "'name': {0},\n'size': {1}".format(self.name, self.size)

		return output

# Extracts the word from a synset... don't know why I can't find this in the documentation
def getWord(synset):
	return synset.name.split(".")[0]

# Outputs an object "node" recursively defined as follows:
#
# node has a string called "name"
# node has a list of nodes called "children"
# leaf nodes do not have children, but an attribute called "size"
def createHeirachy(objFreqDict):

	nodes = [Node(name, size) for name, size in objFreqDict.iteritems() if name is not None]
	(abstractionList, physicalList) = partition(nodes, "abstraction", "physical_entity")
	(organismList, artifactList) = partition(physicalList, "organism", "artifact")

	(weaponList, otherList) = partition(artifactList, "weapon", "other")
	(animalList, peopleList) = partition(organismList, "animal", "other")

	(qualityList, attrList, otherList2) = partition(abstractionList, "quality", "attribute", "other")

	# Now we're packaging up each list into a tree
	qualListLeaves = [JsonNode(qual) for qual in qualityList]
	qualNode = JsonNode(name = "Qualities", children = qualListLeaves)

	attrListLeaves = [JsonNode(qual) for qual in attrList]
	attrNode = JsonNode(name = "Attributes", children = attrListLeaves)

	other2ListLeaves = [JsonNode(qual) for qual in otherList2]
	other2Node = JsonNode(name = "Other2", children = other2ListLeaves)

	abstractParent = JsonNode(name ="Abstract Entities", children = [qualNode, attrNode, other2Node])

	# Other subtree
	weapListLeaves = [JsonNode(node) for node in weaponList]
	weaponNode = JsonNode(name = "Weapons", children = weapListLeaves)

	other1ListLeaves = [JsonNode(node) for node in otherList]
	other1Node = JsonNode(name = "Other1", children = other1ListLeaves)

	nonLivingNode = JsonNode(name = "Not living", children = [weaponNode, other1Node])


	animalListLeaves = [JsonNode(node) for node in animalList]
	animalNode = JsonNode(name = "Animals", children = animalListLeaves)

	peopleList = [JsonNode(node) for node in peopleList]
	peopleNode = JsonNode(name = "People", children = peopleList)

	livingNode = JsonNode(name = "Organism", children = [animalNode, peopleNode])

	# Now parent nodes
	physicalParent = JsonNode(name = "Physical Objects", children =[nonLivingNode, livingNode])

	entityRoot = JsonNode(name = "Entity", children = [abstractParent, physicalParent])

	output = entityRoot.objectRep()

	with open("jsonoutput.json", 'w') as outfile:
		json.dump(output, outfile, indent = 4, ensure_ascii = False)
		print "Created a JSON file"



def partition(nodes, * categories):

	# Gets the synsets of the categories
	if "other" not in categories:
		cats = [wn.synsets(cat, pos=wn.NOUN)[0] for cat in categories]
	else:
		cats = [wn.synsets(cat, pos=wn.NOUN)[0] for cat in categories[:len(categories)-1]]

	# The output list
	output = [[] for cat in categories]

	# Iterate through each node, and try putting it into an output list
	# The structure of the code is different if one of the categories is "other"
	if "other" not in categories:
		for node in nodes:
			try:

				# Iterate through the synsets of the categories
				for i in range(0, len(cats)):
					lchWord = getWord(node.synset.lowest_common_hypernyms(cats[i])[0])
					if lchWord == getWord(cats[i]) or lchWord == getWord(node.synset):
						output[i].append(node)
			except:
				print "Failing on: " + node.name
	else:
		for node in nodes:
			alreadyInserted = False
			try:

				# Iterate through the synsets of the categories
				for i in range(0, len(cats)):

					lchWord = getWord(node.synset.lowest_common_hypernyms(cats[i])[0])
					if lchWord == getWord(cats[i]) or lchWord == getWord(node.synset):
						output[i].append(node)
						alreadyInserted = True

					if i == len(cats)-1 and not alreadyInserted:
						output[i+1].append(node)
			except:
				print "Failing on: " + node.name

	return output
