import nltk, re, pprint
import sys
import json
import os
from nltk.corpus import wordnet as wn
"""
The working horse of MP2. Uses part of speech tagging to figure out
what items King Arthur / Wart 
"""
def getObjFreqDict(book, * names):

	# Parses the text and chunks it.
	posList = getPosList(book)
	
	# For each other parameter in the list, we add its frequency count to the final output
	freqList = dict()
	for name in names:
		newFreqList = getNameFreqList(posList, name)
		if newFreqList is None:
			continue
		freqList = dict(freqList.items() + newFreqList.items())
	return freqList

###############################################################
# For analysis
###############################################################

# Uses the pos list to analyze what "name" has
def getNameFreqList(posList, name):
	output = dict()

	# From here, analyze sentence by sentence
	# Sorry, I wish I knew a neater way of doing this...
	for i in range(0, len(posList)):
		sentence = posList[i];
		newDict = countPossession(sentence, name)
		if newDict is not None:
			output = dict(output.items() + newDict.items())
		newDict = countPossPronouns(posList, name, i)
		if newDict is not None:
			output = dict(output.items() + newDict.items())
	return output

# Uses the part of speech tag "POS" to analyze the sentence
def countPossession(sentence, name):
	indices = hasTag(sentence, 'POS');
	if(len(indices) == 0): 
		return
	
	# indices is a list with the indices of the POS tag
	output = dict()
	for index in indices:
		# Theoretically doesn't happen
		if index == 0:
			continue

		# Check the last index to see who it is
		accName = getWord(sentence, index-1)
		word = getWord(sentence, index+1)
		if accName == name:
			output[word] = output.get(word, 0) + 1
	return output

# Tries to infer what the possessive pronoun belongs to...
# Returns a tuple of (guesses, misses, outputDict)
def countPossPronouns(posList, name, currIndex):

	# Looking for words like "his" or "her"
	indices = hasWord(posList[currIndex], 'his')#hasTag(posList[currIndex], 'PRP$')
	if len(indices) == 0:
		return
	
	# indices is a list with the indices of "him" or etc
	output = dict()
	for hisIndex in indices:
		possibleOwners = getPossOwners(posList, currIndex, hisIndex, name)
		# TODO: CHANGE THIS. Let's just assume that if the name
		# is in the set, then that means yes.
		#
		# The code below will collect the noun phrase
		if name in possibleOwners:
			sentence = posList[currIndex]
			count = 0
			phrase = ""
			tag = sentence[hisIndex+count][1]  # Start with the word after "his"

			try:
				# Obtains the noun that each "his" was modifying
				while True:
					count = count + 1
					currWord = sentence[hisIndex + count][0]
					tag = sentence[hisIndex + count][1]
					if tag == "NN" or tag == "NNS":
						currWord = wn.morphy(currWord)
						output[currWord] = output.get(currWord, 0) + 1
						break
			except:
				print "Failed on sentence {0}: {1}".format(currIndex, sentence)
	return output

# Okay, really complicated. Generates a list of possible indices of nouns
# that could correspond to the possessive pronoun at posList[sentIndex][wordInd]
def getPossOwners(posList, sentIndex, wordInd, name):
	# TODO: Implement a case for females as well


	# Sentence 1: The current sentence
	output = []
	currWord = ("", "")
	count = 0
	sentence = posList[sentIndex]
	while currWord != sentence[wordInd]:
		currWord = sentence[count]
		if currWord[0] == name:
			output.append(currWord[0])
		elif getTag(currWord) == 'NNPS':
			output.append(currWord)
		count = count + 1


	# Sentence 2: The previous sentnce
	if sentIndex - 1 < 0:
		return output
	for word in posList[sentIndex-1]:
		if word[0] == name:
			output.append(word[0])
		elif getTag(word) == 'NNPS':
			output.append(word[0])
	# TODO: Handle a case about he's and she's	
	return output	

# Iterate backwards through the sentence. If you see a noun or proper noun, 
# add it to the list of possibilities
#
# We will stop after seeing two sentence markings


# Gets the word
def getWord(sentence, index):
	return sentence[index][0]

def getTag(word):
	return word[1]
# Tries to find other nouns in the sentence
# that could "own" an object
def possibleOwner(sentence, word, index):
	pass

# Sifts through a pos sentence to check whether
# a word exists. Returns a list of instances where it did
def hasWord(sentence, word):
	output = []
	for index in range(0, len(sentence)):
		if sentence[index][0] == word:
			output.append(index)
	return output

# Sifts through a pos sentence to check whether 
# a tag exists
def hasTag(sentence, tag):
	output = []
	for index in range(0, len(sentence)):
		if sentence[index][1] == tag:
			output.append(index)
	return output

###############################################################
# For opening files
###############################################################


"""
Returns a list of strings that represent chunked items.
"""
def getPosList(text):

	# Keep the POS file for use
	if(os.path.exists(text + ".pos")):
		return getPosObj(text)

	# Otherwise, create one	
	else:
		return createPosFile(text)


# Creates the part of speech file
def createPosFile(text):
	book = open(text + ".txt").read()
	print "Reading the original text file."
	sentences = nltk.sent_tokenize(book)
	sentences = [nltk.word_tokenize(sent) for sent in sentences]
	sentences = [nltk.pos_tag(sent) for sent in sentences]
	with open(text + ".pos", 'w') as outfile:
		json.dump(sentences, outfile, indent = 4, ensure_ascii = False)
		print "Created a pos file."
	return sentences


# Gets and converts a JSON object into the original format
def getPosObj(text):
	posObj = json.load(open(text + ".pos"))
	print "Reading the cached pos file."
	# Converts each word in a sentence into a (string, TAG) pair
	for i in range(0, len(posObj)):
		posObj[i] = [tuple(pair) for pair in posObj[i]]
	return posObj

